package com.amaurote.nanako.domain.user;

public enum UserRole {
    USER, ADMIN
}
