package com.amaurote.nanako.domain.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "User")
public class User {

    @Id
    private String id;

    @Indexed(unique = true)
    private String login;   // browseTag

    private String password;

    @Indexed(unique = true)
    private String email;

    private String fullName;

    private Date dateCreated;

    private List<String> myKanbanIds;

    private UserRole userRole;

    public User() {
    }

    public User(String id, String login, String password, String email, String fullName, Date dateCreated, List<String> myKanbanIds, UserRole userRole) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.fullName = fullName;
        this.dateCreated = dateCreated;
        this.myKanbanIds = myKanbanIds;
        this.userRole = userRole;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<String> getMyKanbanIds() {
        return myKanbanIds;
    }

    public void setMyKanbanIds(List<String> myKanbanIds) {
        this.myKanbanIds = myKanbanIds;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
