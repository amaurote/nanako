package com.amaurote.nanako.domain.kanban;

public class TaskType {

    private String description;
    // TODO private byte[] icon

    public TaskType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
