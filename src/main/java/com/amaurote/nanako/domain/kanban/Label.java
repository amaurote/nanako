package com.amaurote.nanako.domain.kanban;

public class Label {

    private LabelColor textColor;
    private LabelColor backgroundColor;

    public Label(LabelColor textColor, LabelColor backgroundColor) {
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    }

    public LabelColor getTextColor() {
        return textColor;
    }

    public void setTextColor(LabelColor textColor) {
        this.textColor = textColor;
    }

    public LabelColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(LabelColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}


