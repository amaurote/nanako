package com.amaurote.nanako.domain.kanban;

public class LabelColor {
    public int r;
    public int g;
    public int b;

    public LabelColor(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
}
