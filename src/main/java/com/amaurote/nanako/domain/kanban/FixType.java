package com.amaurote.nanako.domain.kanban;

public enum FixType {
    PREFIX, SUFIX, NONE
}
