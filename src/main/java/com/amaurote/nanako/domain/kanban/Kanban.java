package com.amaurote.nanako.domain.kanban;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@Document(collection = "Kanban")
public class Kanban {

    @Id
    private String id;

    private String name;
    private String description;

    private FixType fixType;
    private String fixText;

    private List<StatusColumn> statusColumns;

    private HashMap<String, TaskType> taskTypes;    // type text, TaskType object
    private HashMap<String, Label> labels;          // label text, Label object

    private int taskCounter;

    public Kanban() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FixType getFixType() {
        return fixType;
    }

    public void setFixType(FixType fixType) {
        this.fixType = fixType;
    }

    public String getFixText() {
        return fixText;
    }

    public void setFixText(String fixText) {
        this.fixText = fixText;
    }

    public List<StatusColumn> getStatusColumns() {
        return statusColumns;
    }

    public void setStatusColumns(List<StatusColumn> statusColumns) {
        this.statusColumns = statusColumns;
    }

    public HashMap<String, TaskType> getTaskTypes() {
        return taskTypes;
    }

    public void setTaskTypes(HashMap<String, TaskType> taskTypes) {
        this.taskTypes = taskTypes;
    }

    public HashMap<String, Label> getLabels() {
        return labels;
    }

    public void setLabels(HashMap<String, Label> labels) {
        this.labels = labels;
    }

    public int getTaskCounter() {
        return taskCounter;
    }

    public void setTaskCounter(int taskCounter) {
        this.taskCounter = taskCounter;
    }
}
