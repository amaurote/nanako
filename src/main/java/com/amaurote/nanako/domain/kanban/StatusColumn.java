package com.amaurote.nanako.domain.kanban;

import java.util.List;

public class StatusColumn {

    private String name;
    private String description;

    private int order;

    private List<String> taskIds;

    public StatusColumn(String name, String description, int order, List<String> taskIds) {
        this.name = name;
        this.description = description;
        this.order = order;
        this.taskIds = taskIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<String> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(List<String> taskIds) {
        this.taskIds = taskIds;
    }
}
