package com.amaurote.nanako.domain.task;

public enum TaskPriority {
    HIGHEST, HIGH, MEDIUM, LOW, LOWEST
}
