package com.amaurote.nanako.service;

import com.amaurote.nanako.domain.task.Task;
import com.amaurote.nanako.dto.CommentDTO;
import com.amaurote.nanako.dto.DTOFactory;
import com.amaurote.nanako.dto.TaskDTO;
import com.amaurote.nanako.repository.TaskRepository;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    private final TaskRepository taskRepository;

    private final DTOFactory dtoFactory;

    public TaskService(TaskRepository taskRepository, DTOFactory dtoFactory) {
        this.taskRepository = taskRepository;
        this.dtoFactory = dtoFactory;
    }

    // TASKS
    public TaskDTO getTaskDTOById(String id) {
        // TODO authorisation
        Task task = taskRepository.findById(id).orElse(null);   // TODO make it more effective by repository withdrawn

        return (task != null) ? dtoFactory.createTaskDTO(task) : null;
    }

    public TaskDTO createNewTask(TaskDTO taskDTO) {
        // TODO authorisation

        return null;
    }

    public TaskDTO updateTask(TaskDTO taskDTO) {
        // TODO authorisation

        return null;
    }

    // COMMENTS
    public boolean addComment(String id, CommentDTO comment) {
        // TODO authorisation
        return false;
    }

    public boolean updateComment(String taskId, CommentDTO comment) {
        // TODO authorisation
        return false;
    }

    public boolean deleteComment(String taskId, String commentId) {
        // TODO authorisation
        return false;
    }


}
