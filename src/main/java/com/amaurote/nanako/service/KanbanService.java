package com.amaurote.nanako.service;

import com.amaurote.nanako.domain.kanban.Kanban;
import com.amaurote.nanako.dto.DTOFactory;
import com.amaurote.nanako.dto.KanbanDTO;
import com.amaurote.nanako.repository.KanbanRepository;
import org.springframework.stereotype.Service;

@Service
public class KanbanService {

    private final KanbanRepository kanbanRepository;

    private final DTOFactory dtoFactory;

    public KanbanService(KanbanRepository kanbanRepository, DTOFactory dtoFactory) {
        this.kanbanRepository = kanbanRepository;
        this.dtoFactory = dtoFactory;
    }

    public KanbanDTO getKanbanDTOById(String id) {
        // TODO authorisation
        Kanban kanban = kanbanRepository.findById(id).orElse(null);

        return (kanban != null) ? dtoFactory.createKanbanDTO(kanban) : null;
    }
}
