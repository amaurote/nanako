package com.amaurote.nanako.repository;

import com.amaurote.nanako.domain.task.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends MongoRepository<Task, String> {

    Optional<Task> findById(String id);

    @Query(value = "{'_id' : ?0}", fields = "{'description' : 0, 'kanbanId' : 0,'comments' : 0, 'dateCreated' : 0, 'reporterId' : 0, 'watcherIds' : 0}")
    Task findTaskPreviewById(String id);

    @Query(fields = "{'description' : 0, 'kanbanId' : 0,'comments' : 0, 'dateCreated' : 0, 'reporterId' : 0, 'watcherIds' : 0}")
    List<Task> findAllByIdIn(Iterable<String> id);
}
