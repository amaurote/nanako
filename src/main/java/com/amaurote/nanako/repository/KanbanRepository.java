package com.amaurote.nanako.repository;

import com.amaurote.nanako.domain.kanban.Kanban;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KanbanRepository extends MongoRepository<Kanban, String> {

    Optional<Kanban> findById(String id);
}
