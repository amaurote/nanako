package com.amaurote.nanako.controller;

import com.amaurote.nanako.dto.UserDTO;
import com.amaurote.nanako.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("default")
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public UserDTO getUserDTOById(String id) {

        return null;
    }
}
