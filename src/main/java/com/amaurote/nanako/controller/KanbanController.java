package com.amaurote.nanako.controller;

import com.amaurote.nanako.dto.KanbanDTO;
import com.amaurote.nanako.service.KanbanService;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("default")
@RequestMapping("/kanban")
public class KanbanController {
    private final KanbanService kanbanService;

    public KanbanController(KanbanService kanbanService) {
        this.kanbanService = kanbanService;
    }

    @GetMapping("/{id}")
    public KanbanDTO getKanbanDTOById(@PathVariable("id") String id) {
        return this.kanbanService.getKanbanDTOById(id);
    }

    // put for insert
    // post for update
}
