package com.amaurote.nanako.controller;

import com.amaurote.nanako.dto.CommentDTO;
import com.amaurote.nanako.dto.TaskDTO;
import com.amaurote.nanako.service.TaskService;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("default")
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    // TASKS
    @GetMapping("/{id}")
    public TaskDTO getTaskById(@PathVariable String id) {
        return taskService.getTaskDTOById(id);
    }

    @PutMapping
    public TaskDTO createNewTask(@RequestBody TaskDTO taskDTO) {
        return taskService.createNewTask(taskDTO);
    }

    @PostMapping
    public TaskDTO updateTask(@RequestBody TaskDTO taskDTO) {
        // taskDTO should contains only fields to change + id
        return taskService.updateTask(taskDTO);
    }

    // COMMENTS
    @PutMapping("/{id}/comment")
    public boolean addComment(@PathVariable String id, @RequestBody CommentDTO comment) {
        if(comment == null || comment.getText() == null || comment.getText().equals(""))
            return false;

        return taskService.addComment(id, comment);
    }

    @PostMapping("/{taskId}/comment")
    public boolean updateComment(@PathVariable("taskId") String taskId, @RequestBody CommentDTO comment) {
        if(comment == null || comment.getId() == null || comment.getText() == null || comment.getText().equals(""))
            return false;

        return taskService.updateComment(taskId, comment);
    }

    @DeleteMapping("/{taskId}/comment/{commentId}")
    public boolean deleteComment(@PathVariable("taskId") String taskId, @PathVariable("commentId") String commentId) {
        return taskService.deleteComment(taskId, commentId);
    }
}
