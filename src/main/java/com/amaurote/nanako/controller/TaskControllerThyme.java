package com.amaurote.nanako.controller;

import com.amaurote.nanako.service.TaskService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Profile("thyme")
@RequestMapping("/task")
public class TaskControllerThyme {

    private final TaskService taskService;

    public TaskControllerThyme(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/{id}")
    public String getTaskById(@PathVariable String id, Model model) {
        model.addAttribute("task", taskService.getTaskDTOById(id));

        return "task";
    }
}
