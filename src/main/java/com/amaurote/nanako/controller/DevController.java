package com.amaurote.nanako.controller;

import com.amaurote.nanako.domain.kanban.Kanban;
import com.amaurote.nanako.domain.task.Task;
import com.amaurote.nanako.repository.KanbanRepository;
import com.amaurote.nanako.repository.TaskRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("default")
@RequestMapping("/dev")
public class DevController {

    private final KanbanRepository kanbanRepository;
    private final TaskRepository taskRepository;

    public DevController(KanbanRepository kanbanRepository, TaskRepository taskRepository) {
        this.kanbanRepository = kanbanRepository;
        this.taskRepository = taskRepository;
    }

    @GetMapping("/kanban/{id}")
    public Kanban getKanbanById(@PathVariable("id") String id) {
        return kanbanRepository.findById(id).orElse(null);
    }

    @GetMapping("/task/{id}")
    public Task getTaskById(@PathVariable("id") String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @GetMapping("/taskpreview/{id}")
    public Task getTaskPreviewById(@PathVariable("id") String id) {
        return taskRepository.findTaskPreviewById(id);
    }
}
