package com.amaurote.nanako;

import com.amaurote.nanako.domain.kanban.FixType;
import com.amaurote.nanako.domain.kanban.Kanban;
import com.amaurote.nanako.domain.kanban.Label;
import com.amaurote.nanako.domain.kanban.LabelColor;
import com.amaurote.nanako.domain.kanban.StatusColumn;
import com.amaurote.nanako.domain.kanban.TaskType;
import com.amaurote.nanako.domain.task.Comment;
import com.amaurote.nanako.domain.task.Task;
import com.amaurote.nanako.domain.task.TaskPriority;
import com.amaurote.nanako.domain.user.User;
import com.amaurote.nanako.domain.user.UserRole;
import com.amaurote.nanako.repository.KanbanRepository;
import com.amaurote.nanako.repository.TaskRepository;
import com.amaurote.nanako.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {

    private final KanbanRepository kanbanRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    public DbSeeder(KanbanRepository kanbanRepository, UserRepository userRepository, TaskRepository taskRepository) {
        this.kanbanRepository = kanbanRepository;
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void run(String... args) {

        // Wipe all data
        this.userRepository.deleteAll();
        this.kanbanRepository.deleteAll();
        this.taskRepository.deleteAll();

        // Test Users
        User mainUser = new User("1a2s3d4f5g6", "admin", "admin", "admin@nanako.com", "Tom Järvi", new Date(689392800000L), new ArrayList<>(), UserRole.ADMIN);
        User sampleUser1 = new User("1a2s3d4f5g7", "sample1", "password", "sample1@nanako.com", "Hubert Cumberdale", new Date(), new ArrayList<>(), UserRole.USER);
        User sampleUser2 = new User("1a2s3d4f5g8", "sample2", "password", "sample2@nanako.com", "Jeremy Fisher", new Date(), new ArrayList<>(), UserRole.USER);
        User sampleUser3 = new User("1a2s3d4f5g9", "sample3", "password", "sample3@nanako.com", "Marjory Steward-Baxter", new Date(), new ArrayList<>(), UserRole.USER);

        this.userRepository.saveAll(Arrays.asList(mainUser, sampleUser1, sampleUser2, sampleUser3));

        // Task Types
        List<TaskType> defaultTaskTypes = Arrays.asList(
                new TaskType("Task"),
                new TaskType("Bug Fix"),
                new TaskType("Feature"),
                new TaskType("Non-Dev Task"),
                new TaskType("Design"),
                new TaskType("Database")
        );

        // StatusColumn
        List<StatusColumn> defaultStatusColumns = Arrays.asList(
                new StatusColumn("Open", "Opened tasks without prioritising", 0, new ArrayList<>()),
                new StatusColumn("To Do", "The tasks marked as TO DO", 1, new ArrayList<>()),
                new StatusColumn("In Progress", "Tasks in progress", 2, new ArrayList<>()),
                new StatusColumn("Blocked", "Blocked tasks", 3, new ArrayList<>()),
                new StatusColumn("Closed", "Closed tasks", 4, new ArrayList<>()),
                new StatusColumn("Canceled", "Canceled tasks", 5, new ArrayList<>()));

        // Labels
        List<Label> defaultLabels = Arrays.asList(
                new Label(new LabelColor(255, 255, 255), new LabelColor(37, 37, 161)),
                new Label(new LabelColor(255, 255, 255), new LabelColor(0, 165, 0)),
                new Label(new LabelColor(0, 0, 0), new LabelColor(190, 190, 190)),
                new Label(new LabelColor(0, 0, 0), new LabelColor(255, 220, 0))
        );

        // Test Kanban
        Kanban testKanban = new Kanban();
        testKanban.setName("Nanako Dev Kanban");
        testKanban.setDescription("This is Nanako Kanban for development.");
        testKanban.setFixType(FixType.PREFIX);
        testKanban.setFixText("NANA");
        testKanban.setStatusColumns(defaultStatusColumns);

        HashMap<String, TaskType> taskTypeHashMap = new HashMap<>();
        for (TaskType tt : defaultTaskTypes) {
            taskTypeHashMap.put(tt.getDescription(), tt);
        }
        testKanban.setTaskTypes(taskTypeHashMap);

        HashMap<String, Label> labelHashMap = new HashMap<>();
        labelHashMap.put("programming", defaultLabels.get(0));
        labelHashMap.put("graphics", defaultLabels.get(1));
        labelHashMap.put("articles", defaultLabels.get(2));
        labelHashMap.put("interface", defaultLabels.get(3));
        testKanban.setLabels(labelHashMap);

        this.kanbanRepository.save(testKanban);

        // Test Comments
        Comment testComment1 = new Comment((long) 1, mainUser.getId(), "This is sample comment.", new Date());
        Comment testComment2 = new Comment((long) 2, sampleUser1.getId(), "This is a response to sample comment.", new Date());
        Comment testComment3 = new Comment((long) 3, sampleUser3.getId(), "My comment, right there!", new Date());

        // Test Task
        Task sampleTask1 = new Task();
        sampleTask1.setCount(1);
        sampleTask1.setOrder(0);
        sampleTask1.setKanbanId(testKanban.getId());
        sampleTask1.setCurrentStatus(defaultStatusColumns.get(1).getName());
        sampleTask1.setTitle("The first sample task");
        sampleTask1.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus sem libero, a efficitur augue molestie eu. Nam id malesuada orci. Pellentesque eu pretium mauris. Integer eu sapien sagittis, hendrerit massa non, suscipit sapien. Sed porttitor magna sit amet vestibulum posuere. Ut dapibus velit id sem aliquam consectetur. Nunc viverra nisl vel vulputate pellentesque. Mauris arcu odio, convallis ut pulvinar ut, vestibulum vel diam. Praesent porttitor laoreet nibh euismod accumsan.");
        sampleTask1.setDateCreated(new Date());
        sampleTask1.setReporterId(sampleUser3.getId());
        sampleTask1.setAssignedId(sampleUser2.getId());
        sampleTask1.setWatcherIds(Arrays.asList(mainUser.getId(), sampleUser1.getId(), sampleUser2.getId(), sampleUser3.getId()));
        sampleTask1.setTaskPriority(TaskPriority.MEDIUM);
        sampleTask1.setTaskType("Task");
        sampleTask1.setLabels(Arrays.asList("programming"));
        sampleTask1.setComments(new HashSet<Comment>() {
            {
                add(testComment2);
            }
        });

        sampleTask1 = taskRepository.save(sampleTask1);

        // Test Task 2
        Task sampleTask2 = new Task();
        sampleTask2.setCount(2);
        sampleTask2.setOrder(1);
        sampleTask2.setKanbanId(testKanban.getId());
        sampleTask2.setCurrentStatus(defaultStatusColumns.get(1).getName());
        sampleTask2.setTitle("The second sample task.");
        sampleTask2.setDescription("Nullam ultricies feugiat massa nec pulvinar. Quisque eleifend, metus a condimentum laoreet, nisl neque laoreet metus, id consequat eros mauris vitae dolor. Suspendisse commodo velit eu risus scelerisque, eget egestas dolor dapibus. Morbi consequat risus id placerat venenatis. Nullam hendrerit volutpat mi sed rutrum. Cras arcu mi, tincidunt quis leo lacinia, malesuada tempor tortor. Integer in feugiat turpis. Integer eget finibus est. Curabitur dignissim ex quis mauris laoreet, vitae faucibus nibh sagittis.");
        sampleTask2.setDateCreated(new Date());
        sampleTask2.setReporterId(mainUser.getId());
        sampleTask2.setAssignedId(sampleUser3.getId());
        sampleTask2.setWatcherIds(Arrays.asList(mainUser.getId(), sampleUser2.getId(), sampleUser3.getId()));
        sampleTask2.setTaskPriority(TaskPriority.HIGH);
        sampleTask2.setTaskType("Feature");
        sampleTask2.setLabels(Arrays.asList("programming", "interface"));
        sampleTask2.setComments(new HashSet<Comment>() {
            {
                add(testComment1);
                add(testComment3);
            }
        });

        sampleTask2 = taskRepository.save(sampleTask2);

        testKanban.getStatusColumns().get(1).setTaskIds(Arrays.asList(sampleTask1.getId(), sampleTask2.getId()));
        testKanban.setTaskCounter(3);

        this.kanbanRepository.save(testKanban);

        mainUser.setMyKanbanIds(Arrays.asList(testKanban.getId()));

        this.userRepository.save(mainUser);
    }
}
