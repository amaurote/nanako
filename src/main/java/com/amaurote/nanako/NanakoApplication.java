package com.amaurote.nanako;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NanakoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NanakoApplication.class, args);
    }
}
