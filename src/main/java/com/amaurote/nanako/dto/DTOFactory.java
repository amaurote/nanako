package com.amaurote.nanako.dto;

import com.amaurote.nanako.domain.kanban.FixType;
import com.amaurote.nanako.domain.kanban.Kanban;
import com.amaurote.nanako.domain.kanban.Label;
import com.amaurote.nanako.domain.kanban.StatusColumn;
import com.amaurote.nanako.domain.task.Task;
import com.amaurote.nanako.domain.user.User;
import com.amaurote.nanako.repository.KanbanRepository;
import com.amaurote.nanako.repository.TaskRepository;
import com.amaurote.nanako.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class DTOFactory {

    private final TaskRepository taskRepository;
    private final KanbanRepository kanbanRepository;
    private final UserRepository userRepository;

    public DTOFactory(TaskRepository taskRepository, KanbanRepository kanbanRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.kanbanRepository = kanbanRepository;
        this.userRepository = userRepository;
    }

    public UserDTO createUserDTO(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setFullName(user.getFullName());
        userDTO.setDateCreated(user.getDateCreated());

        return userDTO;
    }

    public KanbanDTO createKanbanDTO(Kanban kanban) {
        KanbanDTO kanbanDTO = new KanbanDTO();

        kanbanDTO.setName(kanban.getName());
        kanbanDTO.setDescription(kanban.getDescription());
        kanbanDTO.setFixType(kanban.getFixType());
        kanbanDTO.setFixText(kanban.getFixText());

        List<KanbanDTO.StatusColumnDTO> statusColumnDTOs = new ArrayList<>();
        for (StatusColumn sc : kanban.getStatusColumns()) {
            KanbanDTO.StatusColumnDTO scdto = new KanbanDTO.StatusColumnDTO();

            scdto.setName(sc.getName());
            scdto.setDescription(sc.getDescription());
            scdto.setOrder(sc.getOrder());

            List<Task> tasks = taskRepository.findAllByIdIn(sc.getTaskIds());

            List<KanbanDTO.TaskPreviewDTO> taskPreviewDTOs = new ArrayList<>();
            for (Task t : tasks) {
                KanbanDTO.TaskPreviewDTO tpdto = new KanbanDTO.TaskPreviewDTO();

                tpdto.setCount(t.getCount());
                tpdto.setOrder(t.getOrder());
                tpdto.setTitle(t.getTitle());

                tpdto.setAssigned(
                        createUserDTO(userRepository.findById(t.getAssignedId()).orElse(null))
                );

                tpdto.setTaskPriority(t.getTaskPriority());
                tpdto.setTaskType(t.getTaskType());
                tpdto.setLabels(t.getLabels());

                taskPreviewDTOs.add(tpdto);
            }

            scdto.setTasks(taskPreviewDTOs);

            statusColumnDTOs.add(scdto);
        }

        kanbanDTO.setStatusColumns(statusColumnDTOs);

        kanbanDTO.setTaskTypes(kanban.getTaskTypes());
        kanbanDTO.setLabels(kanban.getLabels());
        kanbanDTO.setTaskCounter(kanban.getTaskCounter());

        return kanbanDTO;
    }

    public TaskDTO createTaskDTO(Task task) {
        Kanban kanban = kanbanRepository.findById(task.getKanbanId()).orElse(null);
        TaskDTO taskDTO = new TaskDTO();

        if (kanban != null && kanban.getFixType() != FixType.NONE) {
            if (kanban.getFixType() == FixType.PREFIX)
                taskDTO.setName(kanban.getFixText() + "-" + task.getCount());
            else if (kanban.getFixType() == FixType.SUFIX)
                taskDTO.setName(task.getCount() + "-" + kanban.getFixText());
        } else {
            taskDTO.setName(Integer.toString(task.getCount()));
        }

        taskDTO.setKanbanId(task.getKanbanId());
        taskDTO.setCurrentStatus(task.getCurrentStatus());

        taskDTO.setTitle(task.getTitle());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateCreated(task.getDateCreated());

        taskDTO.setReporter(
                createUserDTO(userRepository.findById(task.getReporterId()).orElse(null))
        );
        taskDTO.setAssigned(
                createUserDTO(userRepository.findById(task.getAssignedId()).orElse(null))
        );

        List<UserDTO> watcherDTOs = new ArrayList<>();
        List<User> watchers = (List<User>) userRepository.findAllById(task.getWatcherIds());
        for (User watcher : watchers) {
            watcherDTOs.add(createUserDTO(watcher));
        }
        taskDTO.setWatchers(watcherDTOs);

        taskDTO.setTaskPriority(task.getTaskPriority());
        taskDTO.setTaskTypeText(task.getTaskType());

        if (kanban != null && kanban.getTaskTypes() != null) {
            taskDTO.setTaskType(kanban.getTaskTypes().get(taskDTO.getTaskTypeText()));
        }

        HashMap<String, Label> labels = new HashMap<>();
        if (kanban != null && kanban.getLabels() != null) {
            for (String label : task.getLabels()) {
                labels.put(label, kanban.getLabels().get(label));
            }
        }
        taskDTO.setLabels(labels);

        List<TaskDTO.CommentDTO> commentDTOs = new ArrayList<>();
        if (task.getComments() != null && !task.getComments().isEmpty()) {
            task.getComments().forEach((comment) ->
            {
                // TODO mark owned comment as removable by user (and by admin, don't know yet)
                TaskDTO.CommentDTO commentDTO = new TaskDTO.CommentDTO();
                commentDTO.setId(comment.getId());
                commentDTO.setAuthor(createUserDTO(userRepository.findById(comment.getAuthorId()).orElse(null)));
                commentDTO.setText(comment.getText());
                commentDTO.setDate(comment.getDate());
                commentDTOs.add(commentDTO);
            });
        }
        taskDTO.setComments(commentDTOs);

        return taskDTO;
    }

}
