package com.amaurote.nanako.dto;

import java.util.Date;

public class CommentDTO {

    private Long id;

    private String authorId;
    private String text;
    private Date date;

    public CommentDTO() {
    }

    public CommentDTO(Long id, String authorId, String text, Date date) {
        this.id = id;
        this.authorId = authorId;
        this.text = text;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
