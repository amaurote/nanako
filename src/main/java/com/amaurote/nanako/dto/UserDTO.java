package com.amaurote.nanako.dto;

import java.util.Date;

public class UserDTO {

    private String fullName;
    private Date dateCreated;

    public UserDTO() {
    }

    public UserDTO(String fullName, Date dateCreated) {
        this.fullName = fullName;
        this.dateCreated = dateCreated;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
