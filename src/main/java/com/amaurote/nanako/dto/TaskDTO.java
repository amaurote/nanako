package com.amaurote.nanako.dto;

import com.amaurote.nanako.domain.kanban.Label;
import com.amaurote.nanako.domain.kanban.TaskType;
import com.amaurote.nanako.domain.task.TaskPriority;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TaskDTO {

    public static class CommentDTO {
        private Long id;

        private UserDTO author;
        private String text;
        private Date date;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public UserDTO getAuthor() {
            return author;
        }

        public void setAuthor(UserDTO author) {
            this.author = author;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }

    private String id;

    private String name;

    private String kanbanId;

    private String currentStatus;

    private String title;
    private String description;
    private Date dateCreated;

    private UserDTO reporter;
    private UserDTO assigned;
    private List<UserDTO> watchers;

    private TaskPriority taskPriority;

    private String taskTypeText;
    private TaskType taskType;

    private HashMap<String, Label> labels;

    private List<CommentDTO> comments;

    public TaskDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKanbanId() {
        return kanbanId;
    }

    public void setKanbanId(String kanbanId) {
        this.kanbanId = kanbanId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public UserDTO getReporter() {
        return reporter;
    }

    public void setReporter(UserDTO reporter) {
        this.reporter = reporter;
    }

    public UserDTO getAssigned() {
        return assigned;
    }

    public void setAssigned(UserDTO assigned) {
        this.assigned = assigned;
    }

    public List<UserDTO> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<UserDTO> watchers) {
        this.watchers = watchers;
    }

    public TaskPriority getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(TaskPriority taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getTaskTypeText() {
        return taskTypeText;
    }

    public void setTaskTypeText(String taskTypeText) {
        this.taskTypeText = taskTypeText;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public HashMap<String, Label> getLabels() {
        return labels;
    }

    public void setLabels(HashMap<String, Label> labels) {
        this.labels = labels;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }
}
