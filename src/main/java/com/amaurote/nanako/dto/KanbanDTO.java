package com.amaurote.nanako.dto;

import com.amaurote.nanako.domain.kanban.FixType;
import com.amaurote.nanako.domain.kanban.Label;
import com.amaurote.nanako.domain.kanban.TaskType;
import com.amaurote.nanako.domain.task.TaskPriority;

import java.util.HashMap;
import java.util.List;

public class KanbanDTO {

    public static class StatusColumnDTO {
        private String name;
        private String description;

        private int order;

        private List<TaskPreviewDTO> tasks;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public List<TaskPreviewDTO> getTasks() {
            return tasks;
        }

        public void setTasks(List<TaskPreviewDTO> tasks) {
            this.tasks = tasks;
        }
    }

    public static class TaskPreviewDTO {
        private int count;
        private int order;

        private String title;

        private UserDTO assigned;

        private TaskPriority taskPriority;

        private String taskType;

        private List<String> labels;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public UserDTO getAssigned() {
            return assigned;
        }

        public void setAssigned(UserDTO assigned) {
            this.assigned = assigned;
        }

        public TaskPriority getTaskPriority() {
            return taskPriority;
        }

        public void setTaskPriority(TaskPriority taskPriority) {
            this.taskPriority = taskPriority;
        }

        public String getTaskType() {
            return taskType;
        }

        public void setTaskType(String taskType) {
            this.taskType = taskType;
        }

        public List<String> getLabels() {
            return labels;
        }

        public void setLabels(List<String> labels) {
            this.labels = labels;
        }
    }

    private String name;
    private String description;

    private FixType fixType;
    private String fixText;

    private List<StatusColumnDTO> statusColumns;
    private HashMap<String, TaskType> taskTypes;
    private HashMap<String, Label> labels;

    private int taskCounter;

    public KanbanDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FixType getFixType() {
        return fixType;
    }

    public void setFixType(FixType fixType) {
        this.fixType = fixType;
    }

    public String getFixText() {
        return fixText;
    }

    public void setFixText(String fixText) {
        this.fixText = fixText;
    }

    public List<StatusColumnDTO> getStatusColumns() {
        return statusColumns;
    }

    public void setStatusColumns(List<StatusColumnDTO> statusColumns) {
        this.statusColumns = statusColumns;
    }

    public HashMap<String, TaskType> getTaskTypes() {
        return taskTypes;
    }

    public void setTaskTypes(HashMap<String, TaskType> taskTypes) {
        this.taskTypes = taskTypes;
    }

    public HashMap<String, Label> getLabels() {
        return labels;
    }

    public void setLabels(HashMap<String, Label> labels) {
        this.labels = labels;
    }

    public int getTaskCounter() {
        return taskCounter;
    }

    public void setTaskCounter(int taskCounter) {
        this.taskCounter = taskCounter;
    }
}
